import {PerfAnalyticsLibrary} from "../src";
import {JSDOM} from "jsdom";

window = new JSDOM().window;

window.performance.timing={
    responseStart:0,
    requestStart:0,
    navigationStart:0,
    domContentLoadedEventEnd:0,
};
window.performance.getEntriesByName=function (){
    return [{name:"first-contentful-paint",startTime:0}]
};
window.performance.getEntries=function (){
    return [
        {name:"bootstrap.css",duration:2,initiatorType:"css"},
        {name:"bootstrap.js",duration:5,initiatorType:"script"}
    ]
};
let PerfAnalyticsLibraryObj = new PerfAnalyticsLibrary();
test('Test PerfAnalyticsLibrary Function', (done) => {
    expect(PerfAnalyticsLibraryObj.measureTTFB()).toBe(0);
    expect(PerfAnalyticsLibraryObj.measureFCP()).toBe(0);
    expect(PerfAnalyticsLibraryObj.measureDomLoad()).toBe(0);
    const statics= {
        "bootstrap.js": 5,
        "bootstrap.css": 2
    };
    expect(PerfAnalyticsLibraryObj.measureStatics()).toMatchObject(statics);
    done();
    expect(PerfAnalyticsLibraryObj.measureWindowLoad()).toBe(Date.now());
});




