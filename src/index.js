import axios from "axios";

export class PerfAnalyticsLibrary {

    constructor(serverAddress) {
        this.serverAddress = serverAddress;
    }

    measureTTFB() {
        return window.performance.timing.responseStart - window.performance.timing.requestStart;
    }

    measureFCP() {
        return window.performance.getEntriesByName("first-contentful-paint")[0].startTime;
    }

    measureDomLoad() {
        return window.performance.timing.domContentLoadedEventEnd - window.performance.timing.navigationStart;
    }

    measureWindowLoad() {
        return Date.now() - window.performance.timing.navigationStart;
    }

    measureStatics() {
        let staticsTime = {};
        window.performance.getEntries().forEach(entry => {
            if (entry.initiatorType) {
                if (entry.duration && !/sockjs-node/.test(entry.name)) {
                    staticsTime[entry.name] = entry.duration;
                }
            }
        });
        return staticsTime;
    }

    run() {
        return new Promise((resolve, reject) => {
            const that = this;
            window.addEventListener("load", function () {
                let times = {
                    TTFB: that.measureTTFB(),
                    FCP: that.measureFCP(),
                    DomLoad: that.measureDomLoad(),
                    WindowLoad: that.measureWindowLoad()
                };
                times = Object.assign(times, that.measureStatics());
                axios.post("/perfTime", times,{
                    baseURL:that.serverAddress
                }).then((response) => {
                    resolve(response.data);
                }).catch(error => {
                    reject(error);
                })
            })
        })
    }
}
