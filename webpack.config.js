const path = require('path');

module.exports=
    {
        mode: 'production',
        entry: './src/index.js',
        target:"node",
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'index.js',
            library: 'PerfAnalyticsLibrary',
            libraryTarget: 'commonjs2'
        },
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                    }
                }
            ]
        }
    };
